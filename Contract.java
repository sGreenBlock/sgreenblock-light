/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  Stefan Stolz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import greenfoot.*;  
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.File;
import java.io.FileInputStream;

public abstract class Contract extends Actor implements java.io.Serializable
{
    public String hash;
    
    private long timeStamp;
    private String user;
    BlockChain blockChain;
    
    public Contract(BlockChain blockChain, String user){
        
        this.blockChain = blockChain;
        this.timeStamp = new Date().getTime();
        this.user = user;
        
        this.hash = calculateHash();
        drawContractImage(false,false);
    }
    
    public void drawContractImage(boolean accepted, boolean won){
        String imageFileName = "contract_not_accepted.png";
        if(accepted)
            imageFileName = "contract.png";
        GreenfootImage thisImage = new GreenfootImage(imageFileName);
        //thisImage.drawString(this.hash,4,15);        
        GreenfootImage wonImage = new GreenfootImage("Coin_30.png");
        if(won)
            thisImage.drawImage(wonImage,7,38);
        this.setImage(thisImage);
    }
    
    public abstract boolean isFullfilled();
    
    public void act() 
    {
        this.timeStamp = new Date().getTime();
        this.hash = calculateHash();
        this.user = user;
    }    
    
    public abstract String calculateHash();
    
    public String getUser(){
        return user;
    }
    
    public void setUser(String user){
        this.user = user;
    }
    
    public long getTimeStamp(){
        return timeStamp;
    }
    
    public void setBlockChain(BlockChain blockChain){
        this.blockChain = blockChain;
    }    
    
    public int compareTo(Contract contract){
        if(this.getTimeStamp() > contract.getTimeStamp())
            return 1;
        else if(this.getTimeStamp() < contract.getTimeStamp())
            return -1;
        else
            return 0;
    }
           
}
