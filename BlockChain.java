/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  Stefan Stolz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import greenfoot.*;  
import java.util.ArrayList;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.Map;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * The BlockChain class is used to store the Blockchain of the game.
 * The application is used with the methods of this class. 
 * 
 * @author (st.stolz) 
 */
public class BlockChain extends Actor implements java.io.Serializable
{
    /**
     * The blockChain list holds all blocks which were generated.
     */
    private ArrayList<Block> blockChain; 
    /**
     * The myContractList holds a list of all signed and unsigned 
     * Contract objects which are created by the gamers.
     */
    private LinkedHashMap<String,Contract> myContractList; 
    /**
     * STARTING_DIFFICULTY sets the difficulty level the algorithm 
     * starts to operate. It is recommended to set it to a level 
     * where every gamer has the chance to mine a block, but not to 
     * low. 80 is a good value to start with.  
     */
    private final int STARTING_DIFFICULTY = 10;
    /**
     * AVERAGE_MINING_TIME_S is used to adopt the difficulty level.
     * The average is calculated from the mining time of last 5 blocks.
     */
    private final int AVERAGE_MINING_TIME_S = 5;
    /**
     * DATA_FOLDER holds the name of the folder to store the blocks 
     * and contracts.
     */
    private final String DATA_FOLDER = "blockchain";
    /**
     * user holds the Hash of the username
     */
    private String user;
    
    public BlockChain(String user){
        this.user = Hash_it.getHash(user);
        blockChain = new ArrayList<>(); 
        myContractList = new LinkedHashMap<>();
      
        updateBlockChainFromDisk();
        myContractList = getContractsFromDisk(true);
        updateMyContractsFromChain();
        drawBlockImage();
        
    }
    
    public void drawBlockImage(){        
        GreenfootImage thisImage = new GreenfootImage("purse.png");        
        thisImage.drawString(String.format("%.8s",this.user),20,65);        
        this.setImage(thisImage);
    }
    
    @Override
    protected void addedToWorld(World world){
        redrawBlocksAndContracts();
    }
    
    public void redrawBlocksAndContracts(){
        updateBlockChainFromDisk();
        updateMyContractsFromChain();
        redrawAllBlocks();
        redrawAllContracts();
    }   
    
    public void addContract(int estimatedMinutes, int estimatedBlocks){    
        System.out.println("######### Adding Contract ##########");
        System.out.println("####################################");
        Contract contract = new GuessBlockSize(this,estimatedMinutes*60, estimatedBlocks, this.user);  
        writeContractToDisk(contract);
        myContractList.put(contract.hash,contract);
        updateBlockChainFromDisk();
        updateMyContractsFromChain();
        redrawBlocksAndContracts();
        System.out.println("####################################");
    }
    
    public void mineBlock(){    
        System.out.println("########### Adding Block ###########");
        System.out.println("####################################");
        
        while(true){
            
            LinkedHashMap<String,Contract> notAcceptedContracts = getNotAcceptedContracts();
            if(!(notAcceptedContracts.size() > 0)){
                System.out.println("No Contracts avaiable to mine... Stopping!");
                break;
            }
            else{
                System.out.println("Found Contracts to mine... Starting!");
            }
            String perviousHash = "0";
            int lastDifficulty = 0;
            if(blockChain.size() > 0){
                Block previousBlock = blockChain.get(blockChain.size()-1);
                perviousHash = previousBlock.blockHash;
                lastDifficulty = previousBlock.getDifficulty(); 
            }     
            
            Block newBlock = new Block("created from BlockChain class", perviousHash, notAcceptedContracts,user); 
            
            int increaseDifficultyBy = 1;
            
            if(getAverageMiningTime(5)/1000 > AVERAGE_MINING_TIME_S){
                increaseDifficultyBy = -1;
            }
            
            int difficulty = lastDifficulty+increaseDifficultyBy;
            
            if(difficulty<STARTING_DIFFICULTY){
                difficulty = STARTING_DIFFICULTY;                
            }
            
            Block minedBlock = newBlock.mineBlock(difficulty,this);   
            
            if(minedBlock != null){
                blockChain.add(newBlock);
                drawBlockToWorld(newBlock,blockChain.size()-1);
                System.out.println("Average last 5: \t"+getAverageMiningTime(5)/1000+" s");
                writeChainToDisk();
                updateMyContractsFromChain();
                System.out.println("###################################");
                break;
           }
        }
    }
    
    private long getAverageMiningTime(int n_blocks){
        long miningTimeSum = 0;
        int counter = 0;
        
        if(blockChain.size() <= n_blocks){
            n_blocks = blockChain.size();
        }
        
        for(int i = blockChain.size()-n_blocks; i < blockChain.size();i++,counter++){
                miningTimeSum += blockChain.get(i).getMiningTime();
        }
        //System.out.println("Calculate Average Mining Time: "+miningTimeSum+"/"+counter);
        if(counter == 0)
            return 0;
        return miningTimeSum / counter;
    }
    
    private void redrawAllBlocks(){
        System.out.println("Redraw Blocks");
        World world = getWorld();
        List<Block> allBlocks = world.getObjects(Block.class);
        //System.out.println("Removing all Blocks");
        for(Block block : allBlocks){
            world.removeObject(block);
        }
        //System.out.println("Writing all Blocks to World");
        for(int i = 0; i < blockChain.size();i++){
            //System.out.println("Writing Block: "+(i+1));
            Block block = blockChain.get(i);
            drawBlockToWorld(block,i);
        }        
    }
    
    private void drawBlockToWorld(Block newBlock, int blockNr){
        newBlock.drawBlockImage(); 
        int perviousHeight = 0;
        int perviousWidth = 0;
        int col = blockNr / 5;
        int row = blockNr - col * 5;
        
            Block previousBlock = blockChain.get(0);
            perviousHeight = previousBlock.getImage().getHeight();
            perviousWidth = previousBlock.getImage().getWidth();
               
        World world = getWorld();
        int x = this.getX()+this.getImage().getWidth()+perviousWidth*col;
        int y = this.getY()+((perviousHeight)*(row));
        //System.out.println("Adding block at x: "+x+", y:"+y);
        //System.out.println("perviousHeight: "+perviousHeight);
        //System.out.println("row: "+row);
        //System.out.println("this.getY(): "+this.getY());
        world.addObject(newBlock,x,y);
    }
    
    private void redrawAllContracts(){
        System.out.println("Redraw Contracts");
        World world = getWorld();
        List<Contract> contractsInWorld = world.getObjects(Contract.class);
        //System.out.println("Removing all Contracts from World");
        for(Contract contract : contractsInWorld){
            world.removeObject(contract);
        }
        //System.out.println("Writing all Contracts to World");
        int counter = 0;
        String hashLastContract = null;
        for (Map.Entry<String,Contract> me : myContractList.entrySet()) {
          //System.out.println("Key: "+me.getKey() + " & Value: " + me.getValue());
          drawContractToWorld(me.getValue(),hashLastContract,counter);
          hashLastContract = me.getKey(); 
          counter++;
        }
    }
    
    private void drawContractToWorld(Contract contract, String hashLastContract, int blockNr){
        
        int perviousHeight = 0;
        int perviousWidth = 0;
        int col = blockNr / 4;
        int row = blockNr - col * 4;
        
        Contract previousContract = myContractList.get(hashLastContract);
        perviousHeight = contract.getImage().getHeight();
        perviousWidth = contract.getImage().getWidth();
               
        World world = getWorld();
        int x = this.getX()-perviousWidth-20-(perviousWidth+3)*col;
        int y = this.getY()+((perviousHeight)*(row));
        //System.out.println("Adding block at x: "+x+", y:"+y);
        //System.out.println("perviousHeight: "+perviousHeight);
        //System.out.println("row: "+row);
        //System.out.println("this.getY(): "+this.getY());
        world.addObject(contract,x,y);
    }
    
    private void writeChainToDisk(){
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try{
            fout = new FileOutputStream(DATA_FOLDER+"/"+Hash_it.getHash(this.toString())+".schain");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
            System.out.println("Chain written to disk");
        }
        catch (FileNotFoundException ex) {
            System.out.println("Can not write to disk: "+ex);
        }
        catch (IOException ex) {
            System.out.println("IO error: "+ex);
        }finally {
            if(oos != null){
                //oos.close();
            } 
        }
    }        
    
    private void writeContractToDisk(Contract contract){
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try{
            fout = new FileOutputStream(DATA_FOLDER+"/"+contract.hash+".scontract");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(contract);
            System.out.println("Contract written to disk");
        }
        catch (FileNotFoundException ex) {
            System.out.println("Can not write to disk: "+ex);
        }
        catch (IOException ex) {
            System.out.println("IO error: "+ex);
        }finally {
            if(oos != null){
                //oos.close();
            } 
        }
    } 
        
    private ArrayList<BlockChain> readChainsFromDisk(){
        ArrayList<BlockChain> chainsList = new ArrayList<BlockChain>();
        ObjectInputStream objectinputstream = null;
        final File folder = new File(DATA_FOLDER);
        for (final File fileEntry : folder.listFiles()) {            
            if(fileEntry.isFile() && fileEntry.getName().endsWith(".schain")){
                String filename = fileEntry.getName();
                try {
                    //System.out.println("Reading BlockChain: "+filename);
                    FileInputStream streamIn = new FileInputStream(DATA_FOLDER+"/"+filename);
                    objectinputstream = new ObjectInputStream(streamIn);
                    BlockChain readChain = (BlockChain) objectinputstream.readObject();
                    chainsList.add(readChain);
                    //System.out.println("Reading ready");
                } catch (Exception e) {
                    System.out.println("No Blockchain to read!");
                    e.printStackTrace();
                    
                } finally {
                    if(objectinputstream != null){
                        //objectinputstream .close();
                    } 
                }
            }
        }
        return chainsList;
    }
    
    private LinkedHashMap<String,Contract> getContractsFromDisk(boolean onlyMy){
        LinkedHashMap<String,Contract> contractList = new LinkedHashMap<String,Contract>();
        ObjectInputStream objectinputstream = null;
        final File folder = new File(DATA_FOLDER);
        for (final File fileEntry : folder.listFiles()) {            
            if(fileEntry.isFile() && fileEntry.getName().endsWith(".scontract")){
                String filename = fileEntry.getName();
                try {
                    
                    FileInputStream streamIn = new FileInputStream(DATA_FOLDER+"/"+filename);
                    objectinputstream = new ObjectInputStream(streamIn);
                    Contract readContract = (Contract) objectinputstream.readObject();
                    readContract.setBlockChain(this);
                    //System.out.println("Reading Contract of user "+readContract.getUser()+": "+readContract.hash);
                    if(!onlyMy || readContract.getUser().equals(this.user)){
                        //System.out.println("Putting it to List");
                        readContract.drawContractImage(false,false);
                        contractList.put(readContract.hash,readContract);
                    }
                    //System.out.println("Reading ready");
                } catch (Exception e) {
                    System.out.println("No Contract to read!");
                    e.printStackTrace();
                    
                } finally {
                    if(objectinputstream != null){
                        //objectinputstream .close();
                    } 
                }
            }
        }
        contractList = sortMap(contractList);
        return contractList;
    }
    
    public static LinkedHashMap<String, Contract> sortMap(LinkedHashMap<String, Contract> map) {
        List<Map.Entry<String, Contract>> capitalList = new LinkedList<>(map.entrySet());

        Collections.sort(capitalList, (o1, o2) -> o1.getValue().compareTo(o2.getValue())); 

        LinkedHashMap<String, Contract> result = new LinkedHashMap<>();
        for (Map.Entry<String, Contract> entry : capitalList)
        {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
    
    private void updateMyContractsFromChain(){
        LinkedHashMap<String,Contract> contractList = getContractsFromChain();
        for(Map.Entry<String,Contract> entry : contractList.entrySet()){
            Contract contract = entry.getValue();
            contract.drawContractImage(true,false);
            if(contract.getUser().equals(this.user)){
                //System.out.print("Found contract on disk: ");
                Contract myContract = myContractList.get(entry.getKey());
                if(myContract != null){
                    //System.out.println("updating Image");
                    myContract.drawContractImage(true, false);
                    if(myContract.isFullfilled())
                        myContract.drawContractImage(true, true);
                    
                }
                else{
                    contract.drawContractImage(true, false);
                    if(contract.isFullfilled())
                        contract.drawContractImage(true, true);
                    myContractList.put(contract.hash,contract);                    
                    //System.out.println("adding to list");
                }
            }           
            
        }
    }
    
    public int blockSizeAt(long millis){
        int count = 0;
        //System.out.printf("Searching Blocks until %ss %n",millis);
        for(Block block : blockChain){
            if(block.minedAtTimeStamp() <= millis){
                //System.out.printf("block at %s <= %s %n",new SimpleDateFormat("HH:mm:ss").format(new Date(block.minedAtTimeStamp())),new SimpleDateFormat("HH:mm:ss").format(new Date(millis)));
                count++;
            }            
        }
        return count;
    }
    
    private LinkedHashMap<String,Contract> getContractsFromChain(){
        LinkedHashMap<String,Contract> allContracts = new LinkedHashMap<String,Contract>();
        for(Block block : blockChain){
            LinkedHashMap<String,Contract> contracts = block.getContractList();
            for(Map.Entry<String,Contract> entry : contracts.entrySet()){
                Contract contract = entry.getValue();
                contract.setBlockChain(this);
                allContracts.put(entry.getKey(),contract);
            }
        }
        return allContracts;
    }
    
    private LinkedHashMap<String,Contract> getNotAcceptedContracts(){
        LinkedHashMap<String,Contract> notAcceptedContracts = new LinkedHashMap<String,Contract>();
        LinkedHashMap<String,Contract> allContractsFromChains = getContractsFromChain();
        LinkedHashMap<String,Contract> allContractsFromDisk = getContractsFromDisk(false);
        for(Map.Entry<String,Contract> entry : allContractsFromDisk.entrySet()){
            if(!allContractsFromChains.containsKey(entry.getKey())){
                notAcceptedContracts.put(entry.getKey(),entry.getValue());
                //System.out.println("Found not accepted Contract: "+entry.getKey());
            }
        }
        return notAcceptedContracts;
    }
    
    public boolean updateBlockChainFromDisk(){
        boolean hasChanged = false;
        ArrayList<BlockChain> chainsList = readChainsFromDisk();
        ArrayList<Block> longestOldest = this.getBlockChain();
        
        
        for (BlockChain chain : chainsList) { 
            ArrayList<Block> current = chain.getBlockChain();
            if(current.size() > longestOldest.size()){
                longestOldest = current;
            }
            else if(current.size() == longestOldest.size()){
                if(longestOldest.size() < 1 || current.get(current.size()-1).getMiningTime()<longestOldest.get(longestOldest.size()-1).getMiningTime()){
                    longestOldest = current;
                }
            }            
        }
        if(longestOldest != this.blockChain){
            long time = longestOldest.get(longestOldest.size()-1).getTimeStamp();
            System.out.println("Setting new Chain: ");
            System.out.println("time: "+time);
            System.out.println("size: "+longestOldest.size());
            this.blockChain = longestOldest;            
            hasChanged = true;
        }
        return hasChanged;
    }   
    
    
    public void printChain(){
        
        System.out.printf("%s%n",this.toString());
        
        System.out.printf("########### User sCoins ##########%n");
        System.out.printf("##################################%n");
        
        LinkedHashMap<String,ArrayList<Block>> blocksPerUser = getBlocksPerUser();
        LinkedHashMap<String,ArrayList<Contract>> wonContractsPerUser = getValidContractsPerUser(true);
        LinkedHashMap<String,ArrayList<Contract>> contractsPerUser = getValidContractsPerUser(false);
        HashSet<String> users = getUsers();
        
        int allBlocksN = 0;
        int allContractsN = 0;
        int allWonContractsN = 0;
        int allWonCoins = 0;
        for(String user : users){
            int blocksN = 0;
            ArrayList<Block> blockList = blocksPerUser.get(user);
            if(blockList!=null)
                blocksN = blockList.size();
            allBlocksN += blocksN;
            int wonContractsN = 0;
            ArrayList<Contract> contractList = wonContractsPerUser.get(user);
            if(contractList!=null)
                wonContractsN = contractList.size();
            allWonContractsN += wonContractsN;
            int contractsN = 0;
            contractList = contractsPerUser.get(user);
            if(contractList!=null)
                contractsN = contractList.size();
            allContractsN += contractsN;
            int coinsN = blocksN + wonContractsN;    
            allWonCoins += coinsN;
            System.out.printf("%.8s: %3d sCoins from %3d Blocks and %3d/%-3d won Contracts%n",user,coinsN,blocksN,wonContractsN,contractsN);
        }  
        System.out.printf("----------------------------------%n");
        System.out.printf("%-9s %3d sCoins from %3d Blocks and %3d/%-3d won Contracts%n","makes:",allWonCoins,allBlocksN,allWonContractsN,allContractsN);
        
        System.out.printf("##################################%n");
        
        if(isChainValid()){
            System.out.println("The Chain is valid!");
        }
        else{
            System.out.println("CHAIN CORRUPTED!");
        }
    }
    
    private HashSet<String> getUsers(){
        LinkedHashMap<String,ArrayList<Block>> blocksPerUser = getBlocksPerUser();
        LinkedHashMap<String,ArrayList<Contract>> contractsPerUser = getValidContractsPerUser(false);
        HashSet<String> users = new HashSet<>();
        for(Map.Entry<String, ArrayList<Block>> entry : blocksPerUser.entrySet()){
            users.add(entry.getKey());
        }
        for(Map.Entry<String, ArrayList<Contract>> entry : contractsPerUser.entrySet()){
            users.add(entry.getKey());
        }        
        return users;
    }
    
    private LinkedHashMap<String,ArrayList<Block>> getBlocksPerUser(){
        LinkedHashMap<String,ArrayList<Block>> blocksPerUser = new LinkedHashMap<>();
        for(Block block : blockChain){
            String user = block.getUser();
            ArrayList<Block> userBlockList = blocksPerUser.get(user);
            if(userBlockList == null){
                userBlockList = new ArrayList<>();
            }
            userBlockList.add(block);  
            blocksPerUser.put(user,userBlockList);
        }
        return blocksPerUser;
    }
    
    private LinkedHashMap<String,ArrayList<Contract>> getValidContractsPerUser(boolean won){
        LinkedHashMap<String,ArrayList<Contract>> contractsPerUser = new LinkedHashMap<>();
        for(Map.Entry<String, Contract> entry : getContractsFromChain().entrySet()){
            String user = entry.getValue().getUser();
            ArrayList<Contract> userContractList = contractsPerUser.get(user);
            if(userContractList == null){
                userContractList = new ArrayList<>();
            }
            if(!won || entry.getValue().isFullfilled()){
                userContractList.add(entry.getValue());
            }
            contractsPerUser.put(user,userContractList);
        }
        return contractsPerUser;
    }
    
    
    
    public String toString(){
        String outputStr = "";
        for(Block block : blockChain){
            outputStr += block.toString();
        }
        return outputStr;
    }
    
    /**
     * todo: Also difficulty must be verified
     */
    private boolean isChainValid(){
        boolean returnCheck = true;
        
        Block currentBlock;
        Block previousBlock;
        
        for (int i = 1; i < blockChain.size();i++){
            currentBlock = blockChain.get(i);
            previousBlock = blockChain.get(i-1);
            
            if(!currentBlock.blockHash.equals(currentBlock.calculateBlockHash()) ){
              returnCheck = false;
              System.out.printf("%n currentBlock.hash \t\t: %s",currentBlock.blockHash);
              System.out.printf("%n currentBlock.calculateHash()\t: %s",currentBlock.calculateBlockHash());
            }
            
            if(!previousBlock.blockHash.equals(currentBlock.previousHash) ) {
              returnCheck = false; 
              System.out.printf("%n previousBlock.hash: %s",previousBlock.blockHash);
              System.out.printf("%n currentBlock.previousHash: %s",currentBlock.previousHash);
            }
            
        }
        return returnCheck;
    }
    
    private void setBlockChain(ArrayList<Block> blockChain){
        this.blockChain = blockChain;
    }
    
    private ArrayList<Block> getBlockChain(){
        return this.blockChain;
    }
    
    
    
}
