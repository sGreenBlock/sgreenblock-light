/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  Stefan Stolz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import greenfoot.*;  
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Block extends Actor implements java.io.Serializable
{
    public void act()
    {
        
    }

    public String blockHash;
    public String previousHash;
    private String transactionData; 
    private long timeStamp; //as number of milliseconds since 1/1/1970.
    private long nonce=Long.MIN_VALUE;
    private String user;
    private long miningTime = 0;
    private long minedAtTimeStamp = 0;
    private int difficulty = 0;
    private String merkleRoot;
    private LinkedHashMap<String,Contract> contractList; 

    public Block(String transactionData,String previousHash, LinkedHashMap<String,Contract> contractList, String user ) {
        
        this.transactionData = transactionData;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.user = user;
        this.contractList = contractList;
        this.merkleRoot = calculateMerkleRoot();
        this.blockHash = calculateBlockHash();        
        drawBlockImage();
    }

    public void drawBlockImage(){        
        GreenfootImage thisImage = new GreenfootImage("block_60.png");
        thisImage.drawString(new SimpleDateFormat("HH:mm:ss").format(new Date(minedAtTimeStamp)),3,15);
        long hr = (miningTime/1000)/3600;
        long rem = (miningTime/1000)%3600;
        long mn = rem/60;
        long sec = rem%60;
        String timeString = String.format("%3d:%02d:%02d",hr,mn,sec);
        String infoString = String.format("Difficulty: %3d",difficulty);        
        thisImage.drawString(infoString,3,25);
        infoString = String.format("Time: %s",timeString);        
        thisImage.drawString(infoString,3,35);
        //thisImage.drawString("Block Hash:",2,25);
        //thisImage.drawString(String.format("%.8s",this.hash),2,35);
        thisImage.drawString("User Hash:",3,45);
        thisImage.drawString(String.format("%.8s",this.user),3,55);
        GreenfootImage wonImage = new GreenfootImage("Coin_30.png");
        thisImage.drawImage(wonImage,thisImage.getWidth()-35,thisImage.getHeight()-20);
        this.setImage(thisImage);
    }

    /**
     * Calculates a pseudo Merkle Root by making a Hash over all Contract Hashes.
     * Calculating a real Merkle Root does not bring any benefit but makes code
     * more complicated. If you are interested in programming a real Merkle Tree
     * refer to this ressources:
     * * https://www.youtube.com/watch?v=gUwXCt1qkBU&t=193s
     * * http://www.righto.com/2014/02/bitcoin-mining-hard-way-algorithms.html
     */
    public String calculateMerkleRoot(){
        String stringToHash = getAllContractHashes("");
        return Hash_it.getHash(stringToHash);
    }
    
    /**
     * The Block Hash consists of:
     * 1. The Bitcoin version number. // not implemented here
     * 2. The previous block hash.
     * 3. The Merkle Root of all the transactions selected to be in that block.
     * 4. The timestamp.
     * 5. The difficulty target.
     * 6. The Nonce.
     * https://en.bitcoin.it/wiki/Block_hashing_algorithm
     */
    public String calculateBlockHash(){
        String stringToHash = previousHash + merkleRoot + timeStamp + difficulty + nonce;
        return Hash_it.getHash(stringToHash);
    }
    
    public String toStringContracts(){
        String returnStr = "";
        returnStr += String.format("%n%-15s:","Contracts:");
        returnStr += getAllContractHashes("%n \t\t ");
        return returnStr;
    }
    
    public String getAllContractHashes(String separator){
        String returnStr = "";
        for(Map.Entry<String, Contract> entry : contractList.entrySet()) {
            String key = entry.getKey();
            returnStr += String.format(separator+"%s",key);
        }
        return returnStr;
    }

    public String toString(){
        
        String returnStr = "";
        returnStr += String.format("%n######## Block %.10s ########",blockHash);
        returnStr += String.format("%n##################################");
        returnStr += String.format("%n%-15s: %s","user",user);
        returnStr += String.format("%n%-15s: %s","hash",blockHash);
        returnStr += String.format("%n%-15s: %s","Merkle Root",merkleRoot);
        returnStr += String.format("%n%-15s: %s","previousHash",previousHash);
        returnStr += String.format("%n%-15s: %s","transactionData",transactionData);
        returnStr += String.format("%n%-15s: %s","minedAtTimeStamp",new SimpleDateFormat("HH:mm:ss").format(new Date(minedAtTimeStamp)));
        returnStr += String.format("%n%-15s: %s","miningTime",miningTime);
        returnStr += String.format("%n%-15s: %s","difficulty",difficulty);
        returnStr += String.format("%n%-15s: %s","nonce",nonce);        
        returnStr += toStringContracts();
        returnStr += String.format("%n##################################");
        return returnStr;
    }

    public void setTransactionData(String transactionData){
        this.transactionData = transactionData;
    }

    public Block mineBlock(int difficulty, BlockChain blockchain) {        
        
        System.out.println("Mining difficulty: \t" + difficulty);        
       
        this.difficulty = difficulty;
        
        int hexZeros = difficulty/16;
        System.out.println("Hex Zeros: \t" + hexZeros);
        int remaining = difficulty-(hexZeros*16);
        //System.out.println("remaining: \t" + remaining);
        //int lastHex = (int)(15/(Math.pow(2,remaining)));
        int lastHex = 15 - remaining;
        System.out.println("Hex max: \t" + lastHex);
        Block returnBlock = null;
        long startMiningTime = new Date().getTime();
        
        //String target = new String(new char[hexZeros]).replace('\0', '0'); //Create a string with difficulty * "0"
        //target = target+Integer.toHexString(lastHex);
        long counter = 0;
        while(nonce < Long.MAX_VALUE) {
            if(counter % 500000 == 0){
                System.out.printf("%10d Hashes tried%n",counter);
                if(blockchain.updateBlockChainFromDisk()){                    
                    blockchain.redrawBlocksAndContracts();
                    break;
                }
            }
            nonce = nonce +1;
            long now = new Date().getTime();
            minedAtTimeStamp = now;
            miningTime = now - startMiningTime;
            blockHash = calculateBlockHash();
            counter++;
            String inspectStr = blockHash.substring( 0, hexZeros+1);
            long inspectLong = Long.parseLong(inspectStr,16);
            if(inspectLong <= lastHex){ 
                System.out.printf("%10d tries to find hash%n",counter);
                System.out.println(this);
                returnBlock = this;
                break;
            }
            else{
                //System.out.println("target : " + target + ", hash: " + hash);
            }
        }
        if(returnBlock == null){
            System.out.println("No Block found!");
            nonce = Long.MIN_VALUE;
        }
        System.out.println("Mining over!");
        return returnBlock;
    }
    
    public long getTimeStamp(){
        return timeStamp;
    }
    
    public String getUser(){
        return user;
    }
    
    public long getMiningTime(){
        return miningTime;
    }
    public long minedAtTimeStamp(){
        return minedAtTimeStamp;
    }
    
    public int getDifficulty(){
        return difficulty;
    }
    
    public LinkedHashMap<String,Contract> getContractList(){
        return contractList;
    }
}
