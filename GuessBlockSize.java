/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  Stefan Stolz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import greenfoot.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GuessBlockSize extends Contract
{
    public int estimatedBlocks;
    public int estimatedSeconds;
    
    public GuessBlockSize(BlockChain blockChain, int estimatedSeconds, int estimatedBlocks, String user){
        super(blockChain, user);
        this.blockChain = blockChain;
        this.estimatedBlocks = estimatedBlocks;
        setEstimatedSeconds(estimatedSeconds);
        drawContractImage( false,  false);
    }
    
    public void act() 
    {
        // Add your action code here.
    }        
    
    @Override    
    public void drawContractImage(boolean accepted, boolean won){
        super.drawContractImage(accepted,won);
        GreenfootImage thisImage = getImage();        
        thisImage.drawString(new SimpleDateFormat("HH:mm:ss").format(new Date(super.getTimeStamp()+estimatedSeconds*1000)),4,15);
        thisImage.drawString(""+estimatedBlocks+" Blocks",4,25);
        this.setImage(thisImage);
    }
    
    @Override
    public String calculateHash(){
        String strToHash = super.getTimeStamp() + super.getUser() + estimatedBlocks + estimatedSeconds;
        return Hash_it.getHash(strToHash);
    }
    
    @Override
    public boolean isFullfilled(){        
        long contractTime = this.getTimeStamp();
        long timeStampEstimated = contractTime+(this.getEstimatedSeconds()*1000);
        int blocks = blockChain.blockSizeAt(timeStampEstimated);
        //System.out.printf("Contract for estimated %db Time %ss %s + %ss%n",this.estimatedBlocks(),contractTime,new SimpleDateFormat("HH:mm:ss").format(new Date(contractTime)),this.estimatedSeconds());
        //System.out.printf("Block Size at %ss %s was %sb%n",timeStampEstimated,new SimpleDateFormat("HH:mm:ss").format(new Date(timeStampEstimated)),blocks);
        return (blocks==this.getEstimatedBlocks());
    }
    
    public long getEstimatedSeconds(){
        return estimatedSeconds;
    }
    public void setEstimatedSeconds(int estimatedSeconds){
        if(estimatedSeconds < 60)
            estimatedSeconds = 60;
        this.estimatedSeconds = estimatedSeconds;
    }
    public long getEstimatedBlocks(){
        return estimatedBlocks;
    }
}
