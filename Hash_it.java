/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  Stefan Stolz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.security.*;

public class Hash_it
{

  
  public Hash_it()
  {
  }

  public static String getHash(String strToHash){
    try{
        byte[] bytesOfMessage = strToHash.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] thedigest = md.digest(bytesOfMessage);
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < thedigest.length; i++) {
            String hex = Integer.toHexString(0xff & thedigest[i]);
          	if(hex.length() == 1)
          	     hexString.append('0');
          	hexString.append(hex);
        }
        return hexString.toString();
    }
    catch(Exception e) {
      throw new RuntimeException(e);
    }

  }
  public String myCalculateHash(){
    int result = 17;
    //result = 31 * result + previousHash.hashCode();
    // = 31 * result + (int) timeStamp;
    //result = 31 * result + (int) transactionData.hashCode();
    //result = 31 * result + nonce;
    //result = 31 * result;
    return Integer.toString(result);
  }
}
