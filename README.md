# sGreenBlock Light game - Don't burn your CPUs!

*sGreenBlock Light* is a *Greenfoot* based, simple learning game for Blockchain and Smart Contracts. This README gives some basic information and installation / setup instructions. 

Why is it light? Light, because it simplifies the Blockchain and Contract part for learning purpose. For example it was preferred to use no asymmetric encryption, but to use essential concepts like hashing for usernames and Merkle Tree for the blockchain.

![Demo of sGreenBlock light](images/sGreenBlock-light.gif)

## The game part

It is possible to create Smart Contracts about Blockhain size in a defined time period. Coins are earned for mining and won contracts.

To win, the gamers have to develop strategies to gather as much coins as possible. For guessing of Blockchain size, it is important to observe growing of the blockchain. This is hampered by adoption of the difficulty level and dynamic behavior of other gamers. Gamers which try to win coins by mining will compete primarily with the computer power of other gamers. Is another gamer faster in mining of the next block, all invested time in mining of the same block was wasted. If no one is mining blocks, Contracts will not be approved and will not generate coins. Many users will try to use a mixed strategy of mining and Contract generation. 

## The learning Part

For learning purpose, essential parts of the blockchain algorithm are  implemented very similar to the Bitcoin Blockchain. In contrast to the very basic [Blockchain Education Greenfoot](https://gitlab.com/sGreenBlock/blockchain-education-greenfoot) project, it implements:

* Mining - How is it possible to automatically generate mathematical tasks that are always new
* Automatic adoption of difficulty level to the computer power of attendees
* A finer granular adoption of difficulty level by increasing the predefined part for the Hashcode guessing Bit by Bit.
* Simulating the peer character of real Crypto Currencies
* Coins are calculated dynamically by analyzing the blockchain.

### Details on Peer Sharing of Blockchain in the crowd

The peer character of real Crypto Currencies is simulated by saving Contracts and Blocks with hashed file names in a folder which is synchronized by a cloud provider like Google Drive, Dropbox or similar. This avoids problems like firewalls. The fact that synchroniziation of files is not in real time and influenced by different parameters like Internet bandwidth simulates similar character of the peer character of real Blockchains.

### Inheritance of contracts

The "Guessing of Blockchain size" Contracts is implemented by inherit from abstract class Contract. This way it is possible to use this project to train inheritance in Java programming.

### Concurrency

For Hashcode guessing parallel execution would increase efficiency dramatically. Because it is not implemented in this version, this would offer itself as practical example for concurrent programming. Students can develop ideas for different approaches and benchmark the results. 

## Installation and Setup

### Local for testing

To test it local, you do not have to set up syncing with the help of a provider.

1. install [Greenfoot](https://www.greenfoot.org/download)
2. download current version of sGreenBlock
3. sync the `blockchain` folder by setting up a symlink. In Windows open a
   command promt with administrator privilegs, go to the sGreenBlock folder,
   delete the `blockchain` folder and type in `mklink /D blockchain Target`
   where `Target` is the path to a `blockchain` folder in another sGreenBlock
   copy. In Linux it is very much the same, using the `ln -s` command. 

### Over the Internet

The following guide uses Google Drive for syncing of blockchain. You can adopt it to any other syncing tool like Dropbox or Onedrive.

All attendees have to

* install [Greenfoot](https://www.greenfoot.org/download)
* install [Google Backup & Sync](https://www.google.com/drive/download/backup-and-sync/)

Now log in to *Google Backup & Sync* with an account the teacher is aware of.

1. The Game Leader stores the whole game folder `sGreenBlockLight` in a Google Drive folder
2. The Game Leader sets the game folder `sGreenBlockLight` to permission `Anyone with the link can view` and the subfolder `blockchain` to `Anyone with the link can edit` 
3. Now share the game folder `sGreenBlockLight` to all attendees. This step is easiest if you use [Google Classroom](https://classroom.google.com). 
4. Everyone has to sync the folder `sGreenBlockLight` to his local drive with [Google Backup & Sync](https://www.google.com/drive/download/backup-and-sync/). 
5. Now every attendee goes to the local `sGreenBlockLight` folder and double clicks the `project.greenfoot` file, so that it opens in Greenfoot.

## Gaming Manual

* *Don´t* klick Greenfoot Act or Run Button. It is not needed to play. 
* Create a new `BlockChain` Object by right click on `BlockChain` and selecting `new BlockChain(String user)`
* Type in your user name as String (e.g. "Max"). Keep care to remember your user name. As soon as you klick "ok", it will be hashed and you will not see your user name again. But if you want to use this user again, you have to type in exactly the same name again.
* To play the game you use from this time on only right klick on the bag. Try to use `addContract()` and then `mineBlock()` for the basic functionality.


Refer also to the GIF animation in this README, which shows the basic steps.

## sGreenBlock

sGreenBlock is developed in three versions:

1. [Blockchain Education Greenfoot](https://gitlab.com/sGreenBlock/blockchain-education-greenfoot) - education of blockchain algorithm at low level of programming skills
2. [sGreenBlock Light](https://gitlab.com/sGreenBlock/sgreenblock-light) - learn more about the blockchain and have fun playing with it
3. [sGreenBlock](https://gitlab.com/sGreenBlock/sgreenblock) (pending for
   publication) - learn about implementation details like security aspects
   through asymmetric encryption or how transactions work in detail. This Game
   implements a full Cryptocurrency with Transaction Fees and Smart Contracts
   like Multisign Contracts.

## References

* Some of the basal algorithms are loosely based on the brilliant Java tutorial
[Creating Your First Blockchain with
Java](https://medium.com/programmers-blockchain/create-simple-blockchain-java-tutorial-from-scratch-6eeed3cb03fa).
* Ken Shirriff (2014) Bitcoin mining the hard way: the algorithms, protocols, and bytes. [Online]. Available from: http://www.righto.com/2014/02/bitcoin-mining-hard-way-algorithms.html [Accessed: 18 March 2019].
* Nakamoto, S. (2008) Bitcoin: A peer-to-peer electronic cash system. Bitcoin
Foundation. [Online] Available from: https://bitcoin.org/bitcoin.pdf.





## License

    sGreenBlock light
    Copyright (C) 2018  sGreenBlock

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
